from multiprocessing import Pool

from pdfminer.high_level import extract_text,  extract_pages
from PyPDF2 import PdfFileReader


def load_pdf(filename: str = "Around the World in Eighty Days - Jules Verne - PDF.pdf"):
    pdf_file = open(filename, 'rb')
    with Pool() as p:
        return dict(p.starmap(_load_single_page,
                              [(p, filename) for p in range(PdfFileReader(pdf_file).getNumPages())]))


def _load_single_page(p, filename):
    pdf_file = open(filename, 'rb')
    return p, extract_text(pdf_file, page_numbers=[p])
