"""
Authors:
- Mateusz Sroka
- Dawid Szarek
"""

from datetime import timedelta
import pandas as pd
import twint


if __name__ == '__main__':
    output_file = "data_twint/trump.csv"

    c = twint.Config()
    c.Username = 'realDonaldTrump'
    c.Since = '2020-09-01'
    c.Lang = "en"
    c.Store_csv = True
    c.Output = output_file
    twint.run.Search(c)

    c = twint.Config()
    c.Username = 'realDonaldTrump'
    c.Store_object = True
    c.Custom['user'] = ['location']
    twint.run.Lookup(c)
    df = pd.read_csv(output_file)
    df['location'] = twint.output.users_list[0].location
    df.to_csv(output_file)


    output_file = "data_twint/biden.csv"

    c = twint.Config()
    c.Username = 'JoeBiden'
    c.Since = '2020-09-01'
    c.Lang = "en"
    c.Store_csv = True
    c.Output = output_file
    twint.run.Search(c)

    c = twint.Config()
    c.Username = 'JoeBiden'
    c.Store_object = True
    c.Custom['user'] = ['location']
    twint.run.Lookup(c)
    df = pd.read_csv(output_file)
    df['location'] = twint.output.users_list[0].location
    df.to_csv(output_file)


    date_range = [(str(i.date()), str((i + timedelta(days=1)).date()))
                  for i in pd.date_range(start='2020-09-01', end='2020-11-08', freq='D')]

    for city in ["New York City", "Los Angeles", "Chicago", "Houston", "Phoenix"]:
        for since, until in date_range:
            output_file = f"data_twint/hashtags_{since}_({city}).csv"
            print(since, city)

            c = twint.Config()
            c.Near = city
            c.Since = since
            c.Until = until
            c.Search = '#GE2020 OR #GE20 OR #GeneralElection OR #GeneralElection2020 OR #Election2020'
            c.Lang = "en"
            c.Store_csv = True
            c.Output = output_file
            twint.run.Search(c)
